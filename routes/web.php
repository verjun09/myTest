<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});



Route::get('api/users','UserController@index');
Route::get('api/user/{user_id}','UserController@getUser');
Route::post('api/create','UserController@create');
Route::post('api/update','UserController@update');
Route::post('api/delete','UserController@delete');
