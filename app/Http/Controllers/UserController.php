<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;

class UserController extends Controller
{
	
	/**
		Get All Users
	**/
	public function index(){

		$users = User::all();

		return response()->json([
			'users' => $users
		]);
	}


	/**
		Get Single Users
	**/
	public function getUser($user_id){
		$user = User::find($user_id);
		
		return response()->json([
			'user' => $user
		]);		
	}

	/**
		Create User
	**/
	public function create(Request $request){

        $input = array('firstname' => $request->input('firstname'),
			 'lastname' => $request->input('lastname'),
			 	'email' => $request->input('email'),
			 'username' => $request->input('username'),
			 'password' => $request->input('password'),
			 'phone_no' => $request->input('phone_no'),
			 'postcode' => $request->input('postcode'),
			  'address' => $request->input('address'));

		$rules =  array('firstname' => 'required',
						 'lastname' => 'required',
							'email' => 'required | email',
						 'username' => 'required',
						 'password' => 'required',
						 'phone_no' => 'required | integer',
						 'postcode' => 'required | integer',
						  'address' => 'required');

		$validator = Validator::make($input, $rules);      
       
        if($validator->fails()){
            if(\Request::ajax()){
                return \Response::json(['success' => false, 'errors' => $validator->getMessageBag()]);
            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }else{
        	$user = User::create([
				'firstname' => $request->input('firstname'),
				 'lastname' => $request->input('lastname'),
				 	'email' => $request->input('email'),
				 'username' => $request->input('username'),
				 'password' => $request->input('password'),
				 'phone_no' => $request->input('phone_no'),
				 'postcode' => $request->input('postcode'),
				  'address' => $request->input('address')
			]);

			return  response()->json([
				'success' => true,
				'msg' => "Successfully Added!",
				'user' => $user
			]);	
        }

	}

	/**
		Update User
	**/
	public function update(Request $request)
    {
        $input = array('firstname' => $request->input('firstname'),
			 'lastname' => $request->input('lastname'),
			 	'email' => $request->input('email'),
			 'username' => $request->input('username'),
			 'password' => $request->input('password'),
			 'phone_no' => $request->input('phone_no'),
			 'postcode' => $request->input('postcode'),
			  'address' => $request->input('address'));

		$rules =  array('firstname' => 'required',
						 'lastname' => 'required',
							'email' => 'required | email',
						 'username' => 'required',
						 'password' => 'required',
						 'phone_no' => 'required | integer',
						 'postcode' => 'required | integer',
						  'address' => 'required');

		$validator = Validator::make($input, $rules);      
       
        if($validator->fails()){
            if(\Request::ajax()){
                return \Response::json(['success' => false, 'errors' => $validator->getMessageBag()]);
            }else{
                return \Redirect::back()->withInput()->withErrors($validator);
            }
        }else{

	        $user = User::find($request->input('id'));
	        $user->update($request->all());
	        return response()->json([
	        	'success' => true,
	        	'user' => $user,
	            'message' => 'user updated successfully'
	        ]);
    	}
    }

    /**
		Delete User
	**/
	public function delete(Request $request)
    {
        User::find($request->input('id'))->delete();
        return response()->json([
            'message' => 'User deleted successfully'
        ]);
    }
}
